from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

from news42.data.api import register as data_register

schema_view = get_schema_view(
    openapi.Info(
        title="News42 API",
        default_version='v1',
        description="News42 API",
        terms_of_service="https://news42/legal/tos",
        contact=openapi.Contact(email="legal@news42"),
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
data_register(router)

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path('^api-auth/', include('rest_framework.urls')),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('api/', include(router.urls)),
]
