from rest_framework import serializers, viewsets

from news42.data.models import Source, Category, RawArticle
from news42.registry import register_routes


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = "__all__"


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"


class RawArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = RawArticle
        fields = "__all__"


class SourceViewSet(viewsets.ModelViewSet):
    queryset = Source.objects.all()
    serializer_class = SourceSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_fields = ('name',)


class RawArticleViewSet(viewsets.ModelViewSet):
    queryset = RawArticle.objects.all()
    serializer_class = RawArticleSerializer


def register(router):
    register_routes(router, [
        (r'categories', CategoryViewSet, {}),
        (r'sources', SourceViewSet, {}),
        (r'rawarticles', RawArticleViewSet, {})
    ])
