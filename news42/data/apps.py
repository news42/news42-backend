from django.apps import AppConfig


class DataConfig(AppConfig):
    name = 'news42.data'
