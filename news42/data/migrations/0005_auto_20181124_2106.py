# Generated by Django 2.1.2 on 2018-11-24 21:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0004_rawarticle_url'),
    ]

    operations = [
        migrations.RenameField(
            model_name='rawarticle',
            old_name='url',
            new_name='link',
        ),
    ]
