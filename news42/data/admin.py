from django.contrib import admin

# Register your models here.
from news42.data.models import RawArticle, Source, Category

admin.site.register(RawArticle)
admin.site.register(Source)
admin.site.register(Category)
