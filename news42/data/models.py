from django.db import models


class TimestampMixin(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Source(TimestampMixin):
    name = models.CharField(max_length=255)
    url = models.URLField()

    def __str__(self): return f"{self.name} ({self.url})"


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self): return self.name


class ArticleMetaMixin(TimestampMixin):
    created = models.DateTimeField()
    title = models.CharField(max_length=255)
    author = models.CharField(blank=True, max_length=255)
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    categories = models.ManyToManyField(Category, blank=True)
    link = models.URLField()

    class Meta:
        abstract = True

    def __str__(self): return f"{self.title} ({self.source})"


class RawArticle(ArticleMetaMixin):
    text = models.TextField()
    scraped_at = models.DateTimeField()
