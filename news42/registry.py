def register_routes(router, routes):
    for route in routes:
        prefix, cls, args = route
        router.register(prefix, cls, **args)
