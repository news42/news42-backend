FROM python:latest

RUN mkdir /app
WORKDIR /app
ADD . /app

RUN pip install pipenv uwsgi
RUN pipenv install --system

CMD ["/app/start.sh", "--run"]