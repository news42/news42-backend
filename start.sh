#!/usr/bin/env bash
set -e

if [[ $* == *--run* ]]; then
    # Migrate to Database
    echo '[BACKEND] Running migrate'
    python3 manage.py migrate -v 0 --run-syncdb
    echo '[BACKEND] Running collectstatic'
    python3 manage.py collectstatic -v 0 --clear --noinput
    echo '[BACKEND] Running server'
    uwsgi --env DJANGO_SETTINGS_MODULE=news42.settings --env DJANGO_SECRET_KEY=${DJANGO_SECRET_KEY} --master --http=0.0.0.0:8000 --processes 4 --env DJANGO_CONFIGURATION=Production --module=news42.wsgi:application
elif [[ $* == *--update* ]]; then
    echo "[BACKEND] Updating git repository"
    git pull
    echo "[BACKEND] Shutting down service"
    docker-compose down
    echo "[BACKEND] Pulling new docker images"
    docker-compose pull backend
    echo "[BACKEND] Restarting core services"
    docker-compose up -d
fi



if [ $# -eq 0 ]; then
    echo "[BACKEND] No arguments given"
    exit 1
fi

